CREATE DATABASE laguna OWNER=benjiao;

CREATE TABLE ctmun (
    name varchar(100),
    code integer PRIMARY KEY,
    income_class varchar(5),
    city_class varchar(20),
    district varchar(5),
    voters integer,
    area double precision
);

CREATE TABLE barangay (
    name varchar(255),
    code integer PRIMARY KEY,
    urban_or_rural varchar(6),
    population integer
);

CREATE TABLE alaminos () INHERITS (barangay);
CREATE TABLE liliw () INHERITS (barangay);
CREATE TABLE nagcarlan () INHERITS (barangay);
CREATE TABLE rizal () INHERITS (barangay);
CREATE TABLE sanpablo () INHERITS (barangay);
CREATE TABLE victoria () INHERITS (barangay);

COPY ctmun FROM '/vagrant/data/LE4/ctmun.csv' (FORMAT CSV);
COPY alaminos FROM '/vagrant/data/LE4/alaminos.csv' (FORMAT CSV);
COPY liliw FROM '/vagrant/data/LE4/liliw.csv' (FORMAT CSV);
COPY nagcarlan FROM '/vagrant/data/LE4/nagcarlan.csv' (FORMAT CSV);
COPY rizal FROM '/vagrant/data/LE4/rizal.csv' (FORMAT CSV);
COPY sanpablo FROM '/vagrant/data/LE4/sanpablo.csv' (FORMAT CSV);
COPY victoria FROM '/vagrant/data/LE4/victoria.csv' (FORMAT CSV);