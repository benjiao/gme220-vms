SELECT name, population 
    FROM victoria 
    WHERE population > 2000 
    ORDER BY population ASC;


CREATE VIEW seconddistrict AS
    SELECT *
        FROM ctmun
        WHERE district='2nd';

CREATE VIEW allbrgys_view AS
    SELECT * FROM alaminos
    UNION
    SELECT * FROM liliw
    UNION
    SELECT * FROM nagcarlan
    UNION
    SELECT * FROM rizal
    UNION
    SELECT * FROM sanpablo
    UNION
    SELECT * FROM victoria;

CREATE TABLE allbrgys AS
    SELECT * FROM allbrgys_view;

ALTER TABLE allbrgys
    ADD COLUMN muncode integer;

UPDATE allbrgys SET muncode = (code / 1000) * 1000;

SELECT muncode, SUM(population) AS total_population
    FROM allbrgys
    GROUP BY muncode
    ORDER BY muncode;

SELECT
    c.code, c.name,
    SUM(b.population)  as total_population,
    c.voters,
    (c.voters / CAST(SUM(b.population) AS float)) * 100.0 as voters_pct
FROM ctmun c, allbrgys b
WHERE c.code = b.muncode
GROUP BY c.code
ORDER BY c.code;

SELECT
    c.code, c.name,
    SUM(b.population)  as total_population,
    c.voters,
    (c.voters / CAST(SUM(b.population) AS float)) * 100.0 as voters_pct
FROM ctmun c, allbrgys b
WHERE c.code = b.muncode
GROUP BY c.code
ORDER BY voters_pct DESC
LIMIT 1;


SELECT
    c.code, c.name,
    SUM(b.population) as total_population,
    c.area,
    CAST(SUM(b.population) AS float) / c.area AS persons_per_hectare
FROM ctmun c, allbrgys b
WHERE c.code = b.muncode
GROUP BY c.code
ORDER BY persons_per_hectare ASC
LIMIT 1;


COPY (
    SELECT
        allbrgys.name,
        allbrgys.population,
        pop_counts.total_population,
        (allbrgys.population / pop_counts.total_population) * 100 AS population_pct
    FROM 
        allbrgys,
        (SELECT muncode, CAST(SUM(population) AS float) AS total_population
            FROM allbrgys
            GROUP BY muncode) AS pop_counts
    WHERE pop_counts.muncode=allbrgys.muncode
    ORDER BY name DESC
) TO '/vagrant/data/LE4/population_pct.csv' WITH CSV DELIMITER ',';